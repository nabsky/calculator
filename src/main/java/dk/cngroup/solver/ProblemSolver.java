package dk.cngroup.solver;

import dk.cngroup.model.Problem;
import dk.cngroup.model.ProblemResult;

public interface ProblemSolver {
    ProblemResult solve(Problem problem);
}
