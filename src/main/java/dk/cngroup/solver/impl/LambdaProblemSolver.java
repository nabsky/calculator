package dk.cngroup.solver.impl;

import dk.cngroup.exception.IllegalProblemOperationException;
import dk.cngroup.model.Problem;
import dk.cngroup.model.ProblemResult;
import dk.cngroup.model.ProblemStep;
import dk.cngroup.model.StepType;
import dk.cngroup.solver.ProblemSolver;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class LambdaProblemSolver implements ProblemSolver {

    public LambdaProblemSolver() {
    }

    public ProblemResult solve(Problem problem) {
        ProblemResult result = new ProblemResult();
        if(problem.getStepList() != null && !problem.getStepList().isEmpty()){
            Double value = calculate(problem.getValue(), problem.getStepList());
            result.setValue(value);
        } else {
            result.setValue(problem.getValue());
        }
        return result;
    }

    private Double calculateStep(Double arg1, Double arg2, BiFunction<Double, Double, Double> function){
        return function.apply(arg1, arg2);
    }

    private BiFunction<Double, Double, Double> getProblemStepFunctionByType(StepType type){
        switch (type){
            case ADD:
                BinaryOperator<Double> add = (x, y) -> x + y;
                return add;
            case SUBTRACT:
                BinaryOperator<Double> subtract = (x, y) -> x - y;
                return subtract;
            case MULTIPLY:
                BinaryOperator<Double> mutiply = (x, y) -> x * y;
                return mutiply;
            case DIVIDE:
                BinaryOperator<Double> divide = (x, y) -> x / y;
                return divide;
        }
        throw new IllegalProblemOperationException("Cannot find specified function: " + type.name());
    }

    private Double calculate(Double value, List<ProblemStep> problemStepList){
        if(!problemStepList.isEmpty()){
            ProblemStep problemStep = problemStepList.remove(0);
            value = calculateStep(value, problemStep.getValue(), getProblemStepFunctionByType(problemStep.getType()));
            return calculate(value, problemStepList);
        } else {
            return value;
        }
    }

}
