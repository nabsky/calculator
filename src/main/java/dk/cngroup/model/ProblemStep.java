package dk.cngroup.model;

import dk.cngroup.exception.IllegalProblemOperationException;

public class ProblemStep {
    private StepType type;
    private Double value;

    public StepType getType() {
        return type;
    }

    public Double getValue() {
        return value;
    }

    public ProblemStep(StepType type, Double value) throws IllegalProblemOperationException {
        if(type.equals(StepType.DIVIDE) && value.equals(0.0)){
            throw new IllegalProblemOperationException("Division by zero is not yet supported, sorry!");
        }
        this.type = type;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProblemStep that = (ProblemStep) o;

        if (type != that.type) return false;
        return value != null ? value.equals(that.value) : that.value == null;

    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
