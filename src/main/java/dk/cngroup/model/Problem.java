package dk.cngroup.model;

import java.util.ArrayList;
import java.util.List;

public class Problem {
    private List<ProblemStep> stepList;
    private Double value;

    public void addStep(ProblemStep step){
        if(stepList == null){
            stepList = new ArrayList<ProblemStep>();
        }
        stepList.add(step);
    }

    public List<ProblemStep> getStepList() {
        return stepList;
    }

    public void setStepList(List<ProblemStep> stepList) {
        this.stepList = stepList;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Problem problem = (Problem) o;

        if (stepList != null ? !stepList.equals(problem.stepList) : problem.stepList != null) return false;
        return value.equals(problem.value);

    }

    @Override
    public int hashCode() {
        int result = stepList != null ? stepList.hashCode() : 0;
        result = 31 * result + value.hashCode();
        return result;
    }
}
