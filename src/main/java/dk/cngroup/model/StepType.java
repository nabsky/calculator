package dk.cngroup.model;

import dk.cngroup.exception.IllegalProblemOperationException;

public enum StepType {
    ADD("add"), SUBTRACT("subtract"), MULTIPLY("multiply"), DIVIDE("divide"), APPLY("apply");

    private String type;

    StepType(String type) {
        this.type = type;
    }

    public static StepType fromString(String text) {
        if (text != null) {
            for (StepType stepType : StepType.values()) {
                if (text.equalsIgnoreCase(stepType.type)) {
                    return stepType;
                }
            }
        }
        throw new IllegalProblemOperationException("Operation type '" + text + "' is not defined!");
    }
}
