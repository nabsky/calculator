package dk.cngroup.output.impl;

import dk.cngroup.model.ProblemResult;
import dk.cngroup.output.ResultOutput;

public class ConsoleResultOutput implements ResultOutput {

    public ConsoleResultOutput() {
    }

    public void out(ProblemResult problemResult) {
        System.out.println(problemResult.getValue());
    }

}
