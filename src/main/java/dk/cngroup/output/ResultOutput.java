package dk.cngroup.output;

import dk.cngroup.model.ProblemResult;

public interface ResultOutput {
    void out(ProblemResult problemResult);
}
