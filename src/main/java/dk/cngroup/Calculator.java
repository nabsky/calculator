package dk.cngroup;

import dk.cngroup.exception.IllegalProblemOperationException;
import dk.cngroup.exception.NoSuchProblemException;
import dk.cngroup.exception.WrongProblemStructureException;
import dk.cngroup.model.Problem;
import dk.cngroup.model.ProblemResult;
import dk.cngroup.output.ResultOutput;
import dk.cngroup.output.impl.ConsoleResultOutput;
import dk.cngroup.parser.ProblemParser;
import dk.cngroup.parser.impl.FileProblemParser;
import dk.cngroup.solver.ProblemSolver;
import dk.cngroup.solver.impl.LambdaProblemSolver;

public class Calculator {

    public static void main(String[] args) {
        if (args.length > 0) {
            String filename = args[0];

            ProblemParser problemParser = new FileProblemParser();
            Problem problem = null;
            try {
                problem = problemParser.parse(filename);
            } catch (WrongProblemStructureException e) {
                System.err.println(e.getMessage());
                System.exit(-1);
            } catch (NoSuchProblemException e) {
                System.err.println(e.getMessage());
                System.exit(-2);
            } catch (IllegalProblemOperationException e){
                System.err.println(e.getMessage());
                System.exit(-3);
            }

            Calculator calculator = new Calculator();
            calculator.run(problem);
        } else {
            System.out.println("Please, provide a path to file with problem description!");
        }

    }

    public void run(Problem problem) {
        ProblemSolver problemSolver = new LambdaProblemSolver();
        ProblemResult problemResult = problemSolver.solve(problem);

        ResultOutput resultOutput = new ConsoleResultOutput();
        resultOutput.out(problemResult);
    }

}
