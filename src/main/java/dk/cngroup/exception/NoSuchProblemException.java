package dk.cngroup.exception;

public class NoSuchProblemException extends Throwable {
    public NoSuchProblemException(String message, Throwable cause) {
        super(message, cause);
    }
}
