package dk.cngroup.exception;

public class IllegalProblemOperationException extends RuntimeException {
    public IllegalProblemOperationException(String message) {
        super(message);
    }
}
