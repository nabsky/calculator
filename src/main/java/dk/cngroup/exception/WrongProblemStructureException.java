package dk.cngroup.exception;

public class WrongProblemStructureException extends Throwable {
    public WrongProblemStructureException(String message) {
        super(message);
    }
}
