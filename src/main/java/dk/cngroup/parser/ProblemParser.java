package dk.cngroup.parser;

import dk.cngroup.exception.NoSuchProblemException;
import dk.cngroup.exception.WrongProblemStructureException;
import dk.cngroup.model.Problem;

public interface ProblemParser {
    Problem parse(String source) throws WrongProblemStructureException, NoSuchProblemException;
}
