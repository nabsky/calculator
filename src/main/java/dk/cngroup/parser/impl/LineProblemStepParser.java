package dk.cngroup.parser.impl;

import dk.cngroup.exception.IllegalProblemOperationException;
import dk.cngroup.model.ProblemStep;
import dk.cngroup.model.StepType;
import dk.cngroup.parser.ProblemStepParser;

public class LineProblemStepParser implements ProblemStepParser {

    public ProblemStep parse(String source) {
        String[] strings = source.split(" ");

        if(strings.length!=2){
            throw new IllegalProblemOperationException("Wrong operation format! Operation should contains type and value. You provide: '" + source + "'.");
        }

        StepType stepType = StepType.fromString(strings[0]);

        Double value = null;
        try {
            value = Double.valueOf(strings[1]);
        } catch (NumberFormatException e) {
            throw new IllegalProblemOperationException("Wrong operation format! Value of operation should be a double, but you provide '" + strings[1] + "'.");
        }

        return new ProblemStep(stepType, value);
    }
}
