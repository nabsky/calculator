package dk.cngroup.parser.impl;

import dk.cngroup.exception.IllegalProblemOperationException;
import dk.cngroup.exception.NoSuchProblemException;
import dk.cngroup.exception.WrongProblemStructureException;
import dk.cngroup.model.Problem;
import dk.cngroup.model.ProblemStep;
import dk.cngroup.model.StepType;
import dk.cngroup.parser.ProblemParser;
import dk.cngroup.parser.ProblemStepParser;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileProblemParser implements ProblemParser {

    public FileProblemParser() {
    }

    public Problem parse(String source) throws WrongProblemStructureException, NoSuchProblemException {
        Path path = Paths.get(source);
        Problem problem = new Problem();
        ProblemStepParser problemStepParser = new LineProblemStepParser();
        int index = 0;

        try {
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            for (String line : lines) {
                index++;
                ProblemStep problemStep = problemStepParser.parse(line);
                if (problemStep.getType().equals(StepType.APPLY)) {
                    problem.setValue(problemStep.getValue());
                    if (index != lines.size()) {
                        throw new WrongProblemStructureException("File has a wrong structure! 'Apply' command should be in the end of file but appears at the line " + index);
                    }
                } else {
                    problem.addStep(problemStep);
                }
            }
        } catch (IOException e) {
            throw new NoSuchProblemException("Cannot open the file " + source, e);
        } catch (IllegalProblemOperationException e){
            throw new IllegalProblemOperationException(e.getMessage() + " Please check the line " + index + ".");
        }

        if (problem.getValue() == null) {
            throw new WrongProblemStructureException("File has a wrong structure! 'Apply' command should be in the end of file but it doesn't exists.");
        }

        return problem;
    }
}
