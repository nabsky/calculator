package dk.cngroup.parser;

import dk.cngroup.model.ProblemStep;

public interface ProblemStepParser {
    ProblemStep parse(String source);
}
