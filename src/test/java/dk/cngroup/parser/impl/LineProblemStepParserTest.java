package dk.cngroup.parser.impl;

import dk.cngroup.model.ProblemStep;
import dk.cngroup.model.StepType;
import dk.cngroup.parser.ProblemStepParser;
import org.junit.Assert;
import org.junit.Test;

public class LineProblemStepParserTest {

    @Test
    public void testProvidedLineSamples() {
        ProblemStepParser problemStepParser = new LineProblemStepParser();

        ProblemStep addStep = new ProblemStep(StepType.ADD, 1.0);
        Assert.assertEquals(addStep, problemStepParser.parse("add 1"));

        ProblemStep subtractStep = new ProblemStep(StepType.SUBTRACT, 2.0);
        Assert.assertEquals(subtractStep, problemStepParser.parse("SUBtrACT 2"));

        ProblemStep multiplyStep = new ProblemStep(StepType.MULTIPLY, 3.0);
        Assert.assertEquals(multiplyStep, problemStepParser.parse("multiply 3.0"));

        ProblemStep divideStep = new ProblemStep(StepType.DIVIDE, 4.0);
        Assert.assertEquals(divideStep, problemStepParser.parse("divide 00004"));
    }
}
