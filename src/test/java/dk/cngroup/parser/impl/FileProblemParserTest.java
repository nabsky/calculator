package dk.cngroup.parser.impl;

import dk.cngroup.exception.NoSuchProblemException;
import dk.cngroup.exception.WrongProblemStructureException;
import dk.cngroup.model.Problem;
import dk.cngroup.model.ProblemStep;
import dk.cngroup.model.StepType;
import dk.cngroup.parser.ProblemParser;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class FileProblemParserTest {

    @Test
    public void TestProvidedFileSamples() throws WrongProblemStructureException, NoSuchProblemException {
        ProblemParser problemParser = new FileProblemParser();
        Problem problem = new Problem();

        problem.addStep(new ProblemStep(StepType.ADD, 2.0));
        problem.addStep(new ProblemStep(StepType.MULTIPLY, 3.0));
        problem.setValue(3.0);
        Assert.assertEquals(problem, problemParser.parse("src/test/resources/example1.txt"));

        problem = new Problem();
        problem.addStep(new ProblemStep(StepType.MULTIPLY, 9.0));
        problem.setValue(5.0);
        Assert.assertEquals(problem, problemParser.parse("src/test/resources/example2.txt"));

        problem = new Problem();
        problem.setValue(1.0);
        Assert.assertEquals(problem, problemParser.parse("src/test/resources/example3.txt"));
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testFileNotExists() throws WrongProblemStructureException, NoSuchProblemException {
        expectedException.expect(NoSuchProblemException.class);
        new FileProblemParser().parse("src/test/resources/example.not.exists");
    }
}
