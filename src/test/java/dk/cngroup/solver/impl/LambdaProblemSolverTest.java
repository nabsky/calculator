package dk.cngroup.solver.impl;

import dk.cngroup.exception.IllegalProblemOperationException;
import dk.cngroup.model.Problem;
import dk.cngroup.model.ProblemStep;
import dk.cngroup.model.StepType;
import dk.cngroup.solver.ProblemSolver;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class LambdaProblemSolverTest {

    @Test
    public void testProvidedProblemSamples(){
        ProblemSolver problemSolver = new LambdaProblemSolver();

        Problem problem = new Problem();
        problem.addStep(new ProblemStep(StepType.ADD, 2.0));
        problem.addStep(new ProblemStep(StepType.MULTIPLY, 3.0));
        problem.setValue(3.0);
        Assert.assertEquals(Double.valueOf(15.0), problemSolver.solve(problem).getValue());

        problem = new Problem();
        problem.addStep(new ProblemStep(StepType.MULTIPLY, 9.0));
        problem.setValue(5.0);
        Assert.assertEquals(Double.valueOf(45.0), problemSolver.solve(problem).getValue());

        problem = new Problem();
        problem.setValue(1.0);
        Assert.assertEquals(Double.valueOf(1.0), problemSolver.solve(problem).getValue());
    }

}
