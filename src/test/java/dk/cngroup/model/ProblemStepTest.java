package dk.cngroup.model;

import dk.cngroup.exception.IllegalProblemOperationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ProblemStepTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testDivideOnZero() {
        expectedException.expect(IllegalProblemOperationException.class);
        new ProblemStep(StepType.DIVIDE, 0.0);
    }

}
