package dk.cngroup.model;

import dk.cngroup.exception.IllegalProblemOperationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class StepTypeTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testUnknownOperation() {
        expectedException.expect(IllegalProblemOperationException.class);
        StepType.fromString("unknown");
    }

}
